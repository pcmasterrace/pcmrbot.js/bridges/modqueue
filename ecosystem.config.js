module.exports = {
	apps : [{
		name      : 'bridges/modqueue',
		script    : 'build/modqueue.js',
		env: {
			// Either inject the values via environment variables or define them here
			TRANSPORT_BIND_ADDRESS: process.env.TRANSPORT_BIND_ADDRESS || "",
			BRIDGE_MODQUEUE_SUBREDDIT: process.env.BRIDGE_MODQUEUE_SUBREDDIT || "",
			BRIDGE_MODQUEUE_CHANNEL: process.env.BRIDGE_MODQUEUE_SUBREDDIT || ""
		}
	}]
};
